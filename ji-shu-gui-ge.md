# Quick AI V2技术规格
## 服务端
本软件基于  
`Python3.10`  
`Chantter bot`  
`Flank`
## 网页端
本软件前端部分由`Song Zirui`开发，使用了以下框架:  
`HTML5`  
`CSS`  
`Javascript`
## 官方文档
本软件的文档在开发时使用了[docsify](https://docsify.js.org/)  
源文件托管在[Gitlab](https://gitlab.com/Redish101/quick-ai)  
使用Gitlab提供的服务器
## 运行要求
### 服务端
搭载有Python3.10的Linux设备，请确保Python内有以下模块:
* chantterbot
* flank